package ru.tsc.panteleev.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;

@Getter
@Setter
public class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
