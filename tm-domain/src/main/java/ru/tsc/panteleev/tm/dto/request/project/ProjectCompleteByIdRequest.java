package ru.tsc.panteleev.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectCompleteByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
