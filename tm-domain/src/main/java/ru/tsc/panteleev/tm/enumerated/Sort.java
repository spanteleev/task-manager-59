package ru.tsc.panteleev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Sort {

    BY_NAME("Sort by name", "name"),
    BY_STATUS("Sort by status", "status"),
    BY_CREATED("Sort by created", "created"),
    DATE_BEGIN("Sort by date begin", "dateBegin");

    @Nullable
    private final String displayName;

    @Nullable
    private final String orderColumn;

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(@Nullable final String displayName, @Nullable final String orderColumn) {
        this.displayName = displayName;
        this.orderColumn = orderColumn;
    }

}
