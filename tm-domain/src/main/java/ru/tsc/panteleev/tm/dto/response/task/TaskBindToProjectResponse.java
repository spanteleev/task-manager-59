package ru.tsc.panteleev.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable TaskDto task) {
        super(task);
    }

}
