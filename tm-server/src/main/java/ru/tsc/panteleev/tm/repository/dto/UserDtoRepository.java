package ru.tsc.panteleev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.panteleev.tm.dto.model.UserDto;

import java.util.Collection;
import java.util.List;

@Repository
public class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @Override
    public void set(@NotNull Collection<UserDto> users) {
        clear();
        for (UserDto user : users)
            add(user);
    }

    @NotNull
    @Override
    public List<UserDto> findAll() {
        return entityManager.createQuery("FROM UserDto", UserDto.class).getResultList();
    }

    @Nullable
    @Override
    public UserDto findById(@NotNull String id) {
        return entityManager.find(UserDto.class, id);
    }

    @Nullable
    @Override
    public void removeById(@NotNull String id) {
        remove(findById(id));
    }

    @Override
    public void clear() {
        @NotNull final List<UserDto> users = findAll();
        for (UserDto user : users)
            remove(user);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM UserDto u", Long.class).getSingleResult();
    }

    @Nullable
    @Override
    public UserDto findByLogin(@NotNull String login) {
        List<UserDto> resultList = entityManager.createQuery("SELECT u FROM UserDto u WHERE u.login = :login", UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull String email) {
        List<UserDto> resultList = entityManager.createQuery("SELECT u FROM UserDto u WHERE u.email = :email", UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

}
