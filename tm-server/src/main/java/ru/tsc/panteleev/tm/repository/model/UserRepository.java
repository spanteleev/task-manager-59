package ru.tsc.panteleev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.model.IUserRepository;
import ru.tsc.panteleev.tm.model.User;

import java.util.Collection;
import java.util.List;

@Repository
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void set(@NotNull Collection<User> users) {
        clear();
        for (User user : users)
            add(user);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public void removeById(@NotNull String id) {
        remove(findById(id));
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM User", User.class).executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM User u", Long.class).getSingleResult();
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u WHERE u.login = :login", User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull String email) {
        List<User> resultList = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

}
