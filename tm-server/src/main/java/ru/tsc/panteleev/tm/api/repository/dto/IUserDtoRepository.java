package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.model.UserDto;

import java.util.Collection;
import java.util.List;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    void set(@NotNull Collection<UserDto> users);

    List<UserDto> findAll();

    UserDto findById(@NotNull String id);

    void removeById(@NotNull String id);

    void clear();

    long getSize();

    UserDto findByLogin(@NotNull String login);

    UserDto findByEmail(@NotNull String email);

}
