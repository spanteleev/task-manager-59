package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;

public interface IDtoRepository<M extends AbstractModelDto> {

    void add(@NotNull M model);

    void update(@NotNull final M model);

    void remove(@NotNull final M model);

}
