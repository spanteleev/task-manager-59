package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    void set(@NotNull Collection<ProjectDto> projects);

    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDto> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<ProjectDto> findAll();

    @Nullable
    ProjectDto findById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

    void clearByUserId(@NotNull String userId);

    void clear();

    long getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

}
