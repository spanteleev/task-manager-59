package ru.tsc.panteleev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.panteleev.tm.api.repository.model.IUserRepository;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.model.IUserService;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.entity.UserNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;
import java.util.Collection;
import java.util.List;

@Service
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    protected IUserRepository repository;

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleIncorrectException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<User> users) {
        repository.set(users);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @Transactional
    public void remove(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        repository.remove(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        remove(findByLogin(login));
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    @Transactional
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.update(user);
        return user;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
    }

}
