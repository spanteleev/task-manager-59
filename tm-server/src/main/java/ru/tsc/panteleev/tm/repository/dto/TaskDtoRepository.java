package ru.tsc.panteleev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

@Repository
public class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    @Override
    public void set(@NotNull Collection<TaskDto> tasks) {
        clear();
        for (TaskDto task : tasks)
            add(task);
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM TaskDto t WHERE t.userId = :userId", TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort) {
        if (sort == null) return findAllByUserId(userId);
        @NotNull final String query =
                String.format("SELECT t FROM TaskDto t WHERE t.userId = :userId ORDER BY t.%s", getSortColumn(sort));
        return entityManager.createQuery(query, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDto> findAll() {
        return entityManager.createQuery("FROM TaskDto", TaskDto.class).getResultList();
    }

    @Nullable
    @Override
    public TaskDto findById(@NotNull String userId, @NotNull String id) {
        List<TaskDto> resultList =  entityManager
                .createQuery("SELECT t FROM TaskDto t WHERE t.userId = :userId AND t.id = :id", TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        remove(findById(userId, id));
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        @NotNull final List<TaskDto> tasks = findAllByUserId(userId);
        for (TaskDto task : tasks)
            remove(task);
    }

    @Override
    public void clear() {
        @NotNull final List<TaskDto> tasks = findAll();
        for (TaskDto task : tasks)
            remove(task);
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDto t", Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return findById(userId, id) != null;
    }

    @Override
    public @NotNull List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM TaskDto t WHERE t.userId = :userId AND t.projectId = :projectId", TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<TaskDto> tasks = findAllByProjectId(userId, projectId);
        for (TaskDto task : tasks)
            remove(task);
    }


}
