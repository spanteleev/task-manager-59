package ru.tsc.panteleev.tm.api.repository.dto;

import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDto;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends IDtoRepository<M> {

}
