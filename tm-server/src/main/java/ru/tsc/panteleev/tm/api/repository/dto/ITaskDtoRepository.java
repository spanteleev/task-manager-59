package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    void set(@NotNull Collection<TaskDto> tasks);

    @NotNull
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @NotNull
    List<TaskDto> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<TaskDto> findAll();

    @Nullable
    TaskDto findById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

    void clearByUserId(@NotNull String userId);

    void clear();

    long getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
