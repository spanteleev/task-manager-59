package ru.tsc.panteleev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.model.ITaskRepository;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.enumerated.Sort;
import java.util.Collection;
import java.util.List;

@Repository
public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public void set(@NotNull Collection<Task> tasks) {
        clear();
        for (Task task : tasks)
            add(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM Task t WHERE t.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort) {
        if (sort == null) return findAllByUserId(userId);
        @NotNull final String query =
                String.format("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.%s", getSortColumn(sort));
        return entityManager.createQuery(query, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @Nullable
    @Override
    public Task findById(@NotNull String userId, @NotNull String id) {
        List<Task> resultList = entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        return getFirstRecord(resultList);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        remove(findById(userId, id));
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        entityManager.createQuery("DELETE FROM Task t WHERE t.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task", Task.class).executeUpdate();
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(t) FROM Task t", Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return findById(userId, id) != null;
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        entityManager.
                createQuery("DELETE FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }
}
