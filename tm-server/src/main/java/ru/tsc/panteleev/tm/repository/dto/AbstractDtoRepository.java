package ru.tsc.panteleev.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IDtoRepository;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDto;
import ru.tsc.panteleev.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Getter
@Repository
public abstract class AbstractDtoRepository<M extends AbstractModelDto> implements IDtoRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Nullable
    protected M getFirstRecord(@Nullable final List<M> resultList) {
        return resultList.size()>0 ? resultList.get(0) : null;
    }

    @NotNull
    protected String getSortColumn(@Nullable final Sort sort) {
        return sort.getOrderColumn();
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
