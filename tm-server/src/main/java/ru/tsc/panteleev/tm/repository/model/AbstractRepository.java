package ru.tsc.panteleev.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.model.IRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Getter
@Repository
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Nullable
    protected M getFirstRecord(@Nullable final List<M> resultList) {
        return resultList.size()>0 ? resultList.get(0) : null;
    }

    @NotNull
    protected String getSortColumn(@Nullable final Sort sort) {
        return sort.getOrderColumn();
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
